defmodule LE.SimpleMath do

    import LE.CLILIB

	def compute() do

		IO.puts _compute()

	end

	defp _compute() do
	    number1 = _getNumber("first")
    	number2 = _getNumber("second")
	    "#{number1} + #{number2} = #{_add(number1, number2)}\n" 		<>
        		 		"#{number1} - #{number2} = #{_subtract(number1, number2)}\n" 	<>
        		 		"#{number1} * #{number2} = #{_times(number1, number2)}\n" 		<>
        		 		"#{number1} / #{number2} = #{_divide(number1, number2)}"

	end

	defp _getNumber(order) do
		number = IO.gets "What is the #{order} number? "
		try do
			int_number = toInteger(number)
			validate(int_number)
			int_number
		rescue
			ArgumentError -> 
				IO.puts "Only valid numbers are allowed. Please enter again."
				_getNumber(order)

		end
		
	end

	defp _add(number1, number2), do: number1 + number2
	defp _subtract(number1, number2), do: number1 - number2
	defp _times(number1, number2), do: number1 * number2
	defp _divide(number1, number2), do: number1 / number2


end