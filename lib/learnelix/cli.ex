defmodule LE.CLI do

    @moduledoc """
    Generates a menu for user to choose and run an excerise.
    """

    import LE.SimpleMath

    def run(_argv) do
        IO.puts _argv
        compute()
    end

    def parse_args(argv) do
        parse = OptionParser.parse(argv, switches: [help: :boolean], aliases: [h: :help])
        IO.inspect parse
    end

end