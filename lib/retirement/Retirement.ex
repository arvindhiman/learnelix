defmodule LE.Retirement do

  @moduledoc """
    Calculate year of retirement
  """

    import LE.CLILIB

    def getInput() do

        currentAgeInput = IO.gets "What is your current age? "
        retirementAgeInput = IO.gets "At what age would you like to retire? "

        currentAge = toInteger(currentAgeInput)
        retirementAge = toInteger(retirementAgeInput)

        yearsLeftToRetire = retirementAge - currentAge

        generateOutput yearsLeftToRetire

    end

    def generateOutput(yearsLeftToRetire) when yearsLeftToRetire < 0, do: IO.puts "Congratulations!! You have already retired."

    def generateOutput(yearsLeftToRetire) do
        {{year, _, _}, _} = getCurrentTimestamp()
        IO.puts "You have #{yearsLeftToRetire} years left until you retire."
        IO.puts "It's #{year}, so you can retire in #{year + yearsLeftToRetire}"
    end
end