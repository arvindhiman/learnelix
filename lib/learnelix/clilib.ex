defmodule LE.CLILIB do

    @moduledoc """
    CLI Library
    """

    def toInteger(str_number), do: String.to_integer(String.strip(str_number))
    def validate(number) when number < 0, do: raise ArgumentError
    def validate(number) when number >= 0, do: true
    def getCurrentTimestamp(), do: :os.timestamp |> :calendar.now_to_datetime
end